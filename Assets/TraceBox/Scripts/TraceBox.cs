﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using System;
using System.Collections.Generic;
using Junhee.Utils;

public class TraceBox : MonoSingleton<TraceBox>
{

    // Use this for initialization
    public int limit_line_num = 20;
    public Text text;
    private StringBuilder sb;
    private int line_num = 0;

    private int update_count = 0, old_update_count = 0;

	private void Awake()
	{
		TraceBox.Log("Version. " + Application.version);
	}

	void Start()
    {
		//limit_line_num = int.Parse();
		//text = GetComponentInChildren<Text>();
		//sb = new StringBuilder();
		//TraceBox.Log("Version. " + Application.version);
	}

    public static void Log(params object[] strs)
    {
        if (TraceBox.instance == null) return;
        TraceBox.instance._print(strs);
    }

    private void _print(params object[] strs)
    {
        if (sb == null) sb = new StringBuilder();

        sb.AppendFormat("{0} > ", line_num);
        for (int i = 0; i < strs.Length; i++)
        {
            sb.Append(strs[i].ToString());
            sb.Append(",");
        }
        sb.AppendLine();
        line_num++;

        int cur_line_count = sb.ToString().Split('\n').Length;
        int over_line_num = cur_line_count - limit_line_num;

        if (over_line_num > 0)
        {
            for (int i = 0; i < over_line_num; i++)
            {
                sb.Remove(0, Convert.ToString(sb).Split('\n').Length + 1);
            }
        }

        update_count++;
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			text.gameObject.SetActive(!text.gameObject.activeSelf);
		}

		if (old_update_count != update_count)
        {
            try
            {
                text.text = sb.ToString();
                old_update_count = update_count;
            }
            catch
            {
            }
        }
    }
}

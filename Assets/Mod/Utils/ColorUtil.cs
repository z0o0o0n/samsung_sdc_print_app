﻿namespace Junhee.Utils
{
    using System;
    using System.Collections;
    using UnityEngine;

    public class ColorUtil
    {
        public static Color32 ConvertHexToColor(int hexValue)
        {
            byte R = (byte)((hexValue >> 16) & 0xFF);
            byte G = (byte)((hexValue >> 8) & 0xFF);
            byte B = (byte)((hexValue) & 0xFF);
            
            return new Color32(R, G, B, 255);
        }

        public static Color32 ConvertHexStringToColor(string hexValue)
        {
            return ColorUtil.ConvertHexToColor(Convert.ToInt32(hexValue, 16));
        }
    }
}

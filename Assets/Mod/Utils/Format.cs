﻿namespace Junhee.Utils
{
    using System.Collections;

    public class Format
    {
        // 문자열 길이를 강제로 맞춘다
        // 지정한 길이보다 클 경우 뒤쪽을 자른다
        // 지정한 길이보다 부족할 경우 other 값을 뒤쪽에 추가한다
        // ex)
        // Format.ConvertDigit("12345", 2); // 12
        // Format.ConvertDigit("12", 4, "0"); // 1200
        // Format.ConvertDigit("12", 4); //12
        public static string ConvertDigit(string stringValue, int digit, string other = "", string insertPosition = "front")
        {
            string returnValue;
            if (stringValue.Length > digit)
            {
                returnValue = stringValue.Substring(0, digit);
            }
            else if (stringValue.Length < digit)
            {
                returnValue = stringValue;
                for (int i = 0; i < digit - stringValue.Length; i++)
                {
                    if (insertPosition == "front")
                    {
                        returnValue = returnValue.Insert(0, other);
                    }
                    else if(insertPosition == "back")
                    {
                        returnValue = returnValue.Insert(returnValue.Length, other);
                    }
                }
            }
            else
            {
                returnValue = stringValue;
            }

            return returnValue;
        }
    }
}

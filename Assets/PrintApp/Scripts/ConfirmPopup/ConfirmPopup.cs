﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmPopup : MonoBehaviour 
{
	public delegate void ConfirmPopupEvent();
	public event ConfirmPopupEvent Ok;
	public event ConfirmPopupEvent Cancel;
	public event ConfirmPopupEvent Delete;

	[SerializeField]
	private Image _dimBg;
	[SerializeField]
	private Button _printButton;
	[SerializeField]
	private Button _cancelButton;
	[SerializeField]
	private Button _deleteButton;

	private void Start ()
	{
		Hide();	
	}
	
	private void Update ()
	{
		
	}




	public void OnPrintButtonClick()
	{
		if (Ok != null) Ok();
	}

	public void OnDeleteButtonClick()
	{
		if (Delete != null) Delete();
	}

	public void OnCancelButtonClick()
	{
		if (Cancel != null) Cancel();
	}




	public void Show(string type)
	{
		if(type == "print")
		{
			_printButton.gameObject.SetActive(true);
		}
		else if(type == "delete")
		{
			_deleteButton.gameObject.SetActive(true);
		}

		_dimBg.gameObject.SetActive(true);
		_cancelButton.gameObject.SetActive(true);
	}

	public void Hide()
	{
		_dimBg.gameObject.SetActive(false);
		_printButton.gameObject.SetActive(false);
		_cancelButton.gameObject.SetActive(false);
		_deleteButton.gameObject.SetActive(false);
	}
}

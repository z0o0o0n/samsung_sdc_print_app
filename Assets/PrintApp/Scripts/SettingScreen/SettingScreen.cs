﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingScreen : MonoBehaviour 
{
	[SerializeField]
	private TraceBox _traceBox;
	[SerializeField]
	private Button _traceBoxButton;

	private void Start ()
	{
		DOVirtual.DelayedCall(5f, OnClosed);
	}
	
	private void Update ()
	{
		
	}





	private void OnClosed()
	{
		Hide();
	}
	
	public void OnTraceBoxButtonClick()
	{
		_traceBox.gameObject.SetActive(!_traceBox.gameObject.activeSelf);
	}





	private void Hide()
	{
		_traceBoxButton.gameObject.SetActive(false);
	}
}

﻿using Junhee.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Date : MonoBehaviour 
{
	[SerializeField]
	private Text _text;

	private void Start()
	{
		string y = DateTime.Now.Year.ToString();
		string m = Format.ConvertDigit(DateTime.Now.Month.ToString(), 2, "0");
		string d = Format.ConvertDigit(DateTime.Now.Day.ToString(), 2, "0");
		_text.text = y + "." + m + "." + d;
	}

	private void Update()
	{
		
	}
}

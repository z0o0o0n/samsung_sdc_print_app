﻿using Junhee.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour 
{
	[SerializeField]
	private Text _text;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		string h = Format.ConvertDigit(DateTime.Now.Hour.ToString(), 2, "0");
		string m = Format.ConvertDigit(DateTime.Now.Minute.ToString(), 2, "0");
		string s = Format.ConvertDigit(DateTime.Now.Second.ToString(), 2, "0");
		_text.text = h + " : " + m + " : " + s;
	}
}

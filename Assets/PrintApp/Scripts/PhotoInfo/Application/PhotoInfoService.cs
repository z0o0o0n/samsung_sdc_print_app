﻿using DG.Tweening;
using Junhee.Utils;
using Mod.Net.OSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoInfoService : MonoBehaviour 
{
	public delegate void PhotoInfoServiceEvent();
	public event PhotoInfoServiceEvent Updated;
	public event PhotoInfoServiceEvent PrintRequested;
	public event PhotoInfoServiceEvent PrintEnded;
	public event PhotoInfoServiceEvent DeleteRequested;
	public event PhotoInfoServiceEvent Deleted;

	[SerializeField]
	private PhotoInfo _photoInfo;
	[SerializeField]
	private MulticastClient _multicastClient;

	private void Start ()
	{
		_multicastClient.MessageReceived += OnMessageReceived;
		_photoInfo.Updated += OnPhotoInfoUpdated;
	}
	
	private void Update ()
	{
	}

	private void OnDestroy()
	{
		_multicastClient.MessageReceived -= OnMessageReceived;
		_photoInfo.Updated -= OnPhotoInfoUpdated;
	}





	private void OnMessageReceived(string message, string argument, string senderId)
	{
		if(message == "AddPrintInfo")
		{
			Debug.Log("AddPrintInfo: " + argument);

			if (string.IsNullOrEmpty(argument)) return;

			char[] separator = new char[] { '|' };
			string[] arguments = argument.Split(separator);
			PhotoInfoData photoInfoData = new PhotoInfoData(arguments[0], int.Parse(arguments[1]), arguments[2], arguments[3]);
			_photoInfo.AddPhotoInfoData(photoInfoData);
		}
		else if(message == "ResponsePhotoInfo")
		{
			Debug.Log("ResponsePhotoInfo: " + argument);

			string photoInfoDataListText = argument;
			_photoInfo.SetPhotoInfoDataListByText(photoInfoDataListText);
			if (Updated != null) Updated();
		}
	}

	private void OnPhotoInfoUpdated()
	{
		if (Updated != null) Updated();
	}

	private void OnPrintEnded()
	{
		if (PrintEnded != null) PrintEnded();
	}

	private void OnDeleted()
	{
		RequestItemListOnPC();
		if (Deleted != null) Deleted();
	}





	public List<PhotoInfoData> GetPhotoInfoDataList()
	{
		return _photoInfo.GetPhotoInfoDataList();
	}

	public void RequestItemListOnPC()
	{
		_multicastClient.Send("PrintManager", "RequestPhotoInfo", _photoInfo.limit.ToString());
	}

	public int GetPhotoInfoLimit()
	{
		return _photoInfo.limit;
	}

	public void RequestPrint(string zoneId, int index)
	{
		string filename = "Photo_" + Format.ConvertDigit(index.ToString(), 5, "0") + "_" + zoneId + ".jpg";
		_multicastClient.Send("PrintManager", "RequestPrint", filename);

		DOTween.Kill("PrintProgressDelay");
		DOVirtual.DelayedCall(15f, OnPrintEnded).SetId("PrintProgressDelay");

		if (PrintRequested != null) PrintRequested();
	}

	public void RequestDelete(string zoneId, int index)
	{
		string arguments = zoneId + "|" + index;
		_multicastClient.Send("PrintManager", "RequestDelete", arguments);

		DOTween.Kill("DeleteProgressDelay");
		DOVirtual.DelayedCall(3f, OnDeleted).SetId("DeleteProgressDelay");

		if (DeleteRequested != null) DeleteRequested();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoInfoScrollVeiw : MonoBehaviour 
{
	[SerializeField]
	private GameObject _contentContainer;
	[SerializeField]
	private GameObject _itemPrefab;
	[SerializeField]
	private PhotoInfoService _photoInfoService;
	[SerializeField]
	private ConfirmPopup _confirmPopup;
	private List<PhotoInfoItem> _photoInfoItemList = new List<PhotoInfoItem>();
	private PhotoInfoData _selectedPhotoInfoData;

	private void Start ()
	{
		_photoInfoService.Updated += OnUpdated;
		_confirmPopup.Ok += OnOkButtonClick;
		_confirmPopup.Cancel += OnCancelButtonClick;
		_confirmPopup.Delete += OnDeleteButtonClick;
		CreateItemLists();
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_photoInfoService.Updated -= OnUpdated;
		_confirmPopup.Ok -= OnOkButtonClick;
		_confirmPopup.Cancel -= OnCancelButtonClick;
		_confirmPopup.Delete -= OnDeleteButtonClick;
	}





	private void OnUpdated()
	{
		UpdateListItems();
	}

	private void OnOkButtonClick()
	{
		_photoInfoService.RequestPrint(_selectedPhotoInfoData.zoneId, _selectedPhotoInfoData.index);
		_confirmPopup.Hide();
	}

	private void OnCancelButtonClick()
	{
		_confirmPopup.Hide();
	}

	private void OnDeleteButtonClick()
	{
		_photoInfoService.RequestDelete(_selectedPhotoInfoData.zoneId, _selectedPhotoInfoData.index);
		_confirmPopup.Hide();
	}

	//private void OnItemClick(string zoneId, int index)
	//{
	//	_selectedPhotoInfoData = new PhotoInfoData(zoneId, index, "", "");
	//	_confirmPopup.Show();
	//}

	private void OnPrintItemClick(string zoneId, int index)
	{
		_selectedPhotoInfoData = new PhotoInfoData(zoneId, index, "", "");
		_confirmPopup.Show("print");
	}

	private void OnDeleteItemClick(string zoneId, int index)
	{
		_selectedPhotoInfoData = new PhotoInfoData(zoneId, index, "", "");
		_confirmPopup.Show("delete");
	}





	private void CreateItemLists()
	{
		//_photoInfoService
		_photoInfoService.RequestItemListOnPC();
	}

	private void UpdateListItems()
	{
		RemoveListItems();

		List<PhotoInfoData> photoInfoDataList = _photoInfoService.GetPhotoInfoDataList();

		for (int i = 0; i < photoInfoDataList.Count; i++)
		{
			int index = (photoInfoDataList.Count - 1) - i;

			GameObject instance = Instantiate(_itemPrefab);
			instance.transform.parent = _contentContainer.transform;
			instance.transform.localScale = Vector3.one;
			Vector3 pos = instance.transform.localPosition;
			pos.x = (i * 230f) + 25f;
			pos.y = -300f;
			instance.transform.localPosition = pos;

			PhotoInfoItem photoInfoItem = instance.GetComponent<PhotoInfoItem>();
			photoInfoItem.SetData(photoInfoDataList[index].zoneId, photoInfoDataList[index].index, photoInfoDataList[index].dateString, photoInfoDataList[index].duration);
			photoInfoItem.Print += OnPrintItemClick;
			photoInfoItem.Delete += OnDeleteItemClick;

			_photoInfoItemList.Add(photoInfoItem);
		}

		RectTransform contentContainerRect = _contentContainer.GetComponent<RectTransform>();
		contentContainerRect.sizeDelta = new Vector2((photoInfoDataList.Count * 230f) + 25f, 1080f);
	}

	private void RemoveListItems()
	{
		for(int i = 0; i < _photoInfoItemList.Count; i++)
		{
			_photoInfoItemList[i].Print += OnPrintItemClick;
			_photoInfoItemList[i].Delete += OnDeleteItemClick;
			Destroy(_photoInfoItemList[i].gameObject);
		}
		_photoInfoItemList.Clear();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoInfoItem : MonoBehaviour 
{
	public delegate void PhotoInfoItemEvent(string zondId, int index);
	public event PhotoInfoItemEvent Click;
	public event PhotoInfoItemEvent Print;
	public event PhotoInfoItemEvent Delete;

	[SerializeField]
	private Text _zoneText;
	[SerializeField]
	private Text _timeText;
	[SerializeField]
	private Text _duractionText;
	private int _index;
	private string _zoneId;
	private string _duration;
	private string _dateString;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	//public void OnButtonClick()
	//{
	//	if (Click != null) Click(_zoneId, _index);
	//}

	public void OnPrintButtonClick()
	{
		if (Print != null) Print(_zoneId, _index);
	}

	public void OnDeleteButtonClick()
	{
		if (Delete != null) Delete(_zoneId, _index);
	}





	public void SetData(string zoneId, int index, string dateString, string duration)
	{
		_index = index;
		_zoneId = zoneId;

		string[] splitedDateString = dateString.Split(new char[] { ':' });
		_dateString = splitedDateString[0] + ":" + splitedDateString[1];

		string[] splitedDuration = duration.Split(new char[] { ':' });
		_duration = splitedDuration[0] + "' " + splitedDuration[1] + "\"";

		_zoneText.text = zoneId;
		_timeText.text = _dateString;
		_duractionText.text = _duration;
	}
}

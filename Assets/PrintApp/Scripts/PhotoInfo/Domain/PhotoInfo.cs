﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoInfo : MonoBehaviour 
{
	public delegate void PhotoInfoEvent();
	public event PhotoInfoEvent Updated;

	private int _limit = 30;
	private List<PhotoInfoData> _photoInfoDataList = new List<PhotoInfoData>();

	public int limit
	{
		get { return _limit; }
	}





	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	public void AddPhotoInfoData(PhotoInfoData photoInfoData)
	{
		_photoInfoDataList.Add(photoInfoData);

		if(_photoInfoDataList.Count > _limit)
		{
			_photoInfoDataList.RemoveAt(0);
		}
		TraceBox.Log("add photo info data / index: " + photoInfoData.index + " / zondId: " + photoInfoData.zoneId + " / dateString: " + photoInfoData.dateString + " / listCount: " + _photoInfoDataList.Count);

		if (Updated != null) Updated();
	}

	public List<PhotoInfoData> GetPhotoInfoDataList()
	{
		return _photoInfoDataList;
	}

	public void SetPhotoInfoDataListByText(string text)
	{
		if (string.IsNullOrEmpty(text))
		{
			_photoInfoDataList.Clear();
			return;
		}

		if (_photoInfoDataList != null) _photoInfoDataList.Clear();
		_photoInfoDataList = new List<PhotoInfoData>();

		char[] separator = new char[] { '|' };
		string[] photoInfoItemTextList = text.Split(separator);

		separator = new char[] { ',' };
		for(int i = 0; i < photoInfoItemTextList.Length; i++)
		{
			string[] photoInfoItemText = photoInfoItemTextList[i].Split(separator);
			PhotoInfoData photoInfoData = new PhotoInfoData(photoInfoItemText[0], int.Parse(photoInfoItemText[1]), photoInfoItemText[2], photoInfoItemText[3]);
			_photoInfoDataList.Add(photoInfoData);
		}
	}
}

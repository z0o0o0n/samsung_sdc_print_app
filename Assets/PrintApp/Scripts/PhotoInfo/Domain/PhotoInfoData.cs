﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoInfoData
{
	private string _zondId;
	private int _index;
	private string _dateString;
	private string _duration;

	public string zoneId
	{
		get { return _zondId; }
	}

	public int index
	{
		get { return _index; }
	}

	public string dateString
	{
		get { return _dateString; }
	}

	public string duration
	{
		get { return _duration; }
	}

	public PhotoInfoData(string zondId, int index, string dateString, string duration)
	{
		_zondId = zondId;
		_index = index;
		_dateString = dateString;
		_duration = duration;
	}
}

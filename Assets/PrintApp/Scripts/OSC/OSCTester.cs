﻿using Mod.Net.OSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCTester : MonoBehaviour 
{
	[SerializeField]
	private MulticastClient _multicastClient;
	[SerializeField]
	private PhotoInfoService _photoInfoService;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		
	}





	public void OnConnectionButtonClick()
	{
		_multicastClient.Connect();
	}

	public void OnSendButtonClick()
	{
		_multicastClient.Send("PrintManager", "HI PC", "I'm Phone.");
	}

	public void OnUpdateButtonClick()
	{
		_multicastClient.Send("PrintManager", "RequestPhotoInfo", _photoInfoService.GetPhotoInfoLimit().ToString());
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressScreen : MonoBehaviour
{
	[SerializeField]
	private Image _bg;
	[SerializeField]
	private Text _progressText;
	[SerializeField]
	private PhotoInfoService _photoInfoService;

	private void Start ()
	{
		_photoInfoService.PrintRequested += OnPrintRequested;
		_photoInfoService.PrintEnded += OnPrintEnded;
		_photoInfoService.DeleteRequested += OnDeleteRequested;
		_photoInfoService.Deleted += OnDeleted;
		Hide();	
	}
	
	private void Update ()
	{
		
	}

	private void OnDestroy()
	{
		_photoInfoService.PrintRequested -= OnPrintRequested;
		_photoInfoService.PrintEnded -= OnPrintEnded;
		_photoInfoService.DeleteRequested -= OnDeleteRequested;
		_photoInfoService.Deleted -= OnDeleted;
	}





	private void OnPrintRequested()
	{
		_progressText.text = "Printing...";
		Show();
	}

	private void OnPrintEnded()
	{
		Hide();
	}

	private void OnDeleteRequested()
	{
		_progressText.text = "Deleting...";
		Show();
	}

	private void OnDeleted()
	{
		Hide();
	}





	private void Show()
	{
		_bg.gameObject.SetActive(true);
		_progressText.gameObject.SetActive(true);
	}

	private void Hide()
	{
		_bg.gameObject.SetActive(false);
		_progressText.gameObject.SetActive(false);
	}
}

